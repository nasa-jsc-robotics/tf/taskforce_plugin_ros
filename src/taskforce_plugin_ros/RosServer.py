from threading import Thread
from Queue import Queue
import json
import rospy
import time

from taskforce_common import Command, Server
# due to name conflict, we need to import with different name
from taskforce_plugin_ros_msgs.srv import Command as CommandService
from taskforce_plugin_ros_msgs.srv import CommandResponse

import logging

ROS_SERVICE_TIMEOUT = 3


class RosServerNode(Thread):
    """
    The RosServerNode is a Thread created by the RosServer.
    """

    STOP_PROCESS = '\n'

    def __init__(self, name, requestQueue, responseQueue, commandQueue):
        Thread.__init__(self)
        self.logger = logging.getLogger(name)
        self.name = name
        self.requestQueue = requestQueue
        self.responseQueue = responseQueue
        self.commandQueue = commandQueue
        self.running = False
        self.service = None

    def run(self):
        self.running = True
        self.service = rospy.Service(self.name, CommandService, self._incomingRequest)

        while self.running:
            if not self.commandQueue.empty():
                break
            time.sleep(0.1)
        self.service.shutdown('Service "{}" shutting down'.format(self.name))

    def stop(self):
        self.running = False

    def _incomingRequest(self, request):
        """
        Service an incoming ROS Service request.

        This method is registered as the callback by the internal ROS Service, and should not be called explicitly.

        Args:
            request: taskforce_plugin_ros_msgs.srv.Command service message

        Returns:
            taskforce_plugin_ros_msgs.srv.CommandResponse message.
        """
        if request.args == '':
            args = ()
        else:
            args = json.loads(request.args)

        if request.kwargs == '':
            kwargs = {}
        else:
            kwargs = json.loads(request.kwargs)

        # Convert the incoming request as a taskforce_common.Command object, and put it in the queue.
        command = Command(name=request.name,
                          source=request.source,
                          destination=request.destination,
                          args=args,
                          kwargs=kwargs)
        self.requestQueue.put(command)

        # block until we get a response
        response = self.responseQueue.get(True)

        # Build and send a response
        rep = CommandResponse()
        rep.name = str(response.name)
        rep.source = str(response.source)
        rep.destination = str(response.destination)
        rep.data = json.dumps(response.data, sort_keys=True)
        return rep


class RosServer(Server):
    """
    The RosServer provides a ROS Service interface wherever a taskforce_common.Server object is used.  The command
    and response messages used by the service is defined in taskforce_plugin_ros_msgs.srv.Command.

    Due to the way Server ports are defined and how ROS Services use registered callbacks.  Note that this class
    is NOT the actual ROS Node.  Due to the fine details of how ROS nodes and processes work, the ROS Node to handle
    the service needs to be in a different process.
    """

    def __init__(self, name):
        """
        Args:
            name: Name of the ROS Service
        """
        super(RosServer, self).__init__()
        self.name = name
        self.serviceProcess = None
        self.requestQueue = Queue()
        self.responseQueue = Queue()
        self.commandQueue = Queue()

        self.initialized = False
        self.connected = False

    def initialize(self):
        """
        Returns:
            True on success.  If this object has already been initialized, this method returns False.
        """
        if not self.initialized:
            self.initialized = True
            return True
        return False

    def connect(self):
        """
        Connect the Server.  This will create the ROS Node in another process.

        Returns:
            True on success.  If this object has already been connected, this method returns False.
        """
        if not self.connected:
            if not self.initialized:
                init_ok = self.initialize()
                if not init_ok:
                    return False

            self.serviceProcess = RosServerNode(self.name, self.requestQueue, self.responseQueue, self.commandQueue)
            self.serviceProcess.daemon = True
            self.serviceProcess.start()
            self.connected = True
            return True
        return False

    def disconnect(self):
        """
        Disconnect the server.  This will shutdown the ROS Node.
        Returns:
            True on success.  If this object is not connected, this method returns False.
        """
        if self.connected:
            if self.serviceProcess.is_alive():
                self.commandQueue.put(RosServerNode.STOP_PROCESS)
                self.serviceProcess.join(ROS_SERVICE_TIMEOUT)
            self.serviceProcess = None
            self.connected = False
        return False

    def waitForRequest(self):
        """
        Check for incoming service requests.

        Returns:
            If there are queued incoming requests, return the first request in the queue, otherwise return None.
        """
        if not self.connected:
            connect_ok = self.connect()
            if not connect_ok:
                return False

        if not self.requestQueue.empty():
            return self.requestQueue.get()
        else:
            return None

    def reply(self, message):
        """
        Reply to the Server request.

        Args:
            message: Service reply as a taskforce_common.Message
        """
        self.responseQueue.put(message)
