import rospy
from .utils import configureRosLogging

import logging


class RosTaskEngineNode(object):

    def __init__(self, name="TaskEngine"):

        if rospy.get_node_uri() is None:
            configureRosLogging()

        logging.info("Starting RosTaskEngineNode:{}".format(name))
        rospy.init_node(name, anonymous=False, disable_signals=True)
