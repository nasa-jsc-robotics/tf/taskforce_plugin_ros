"""
RosTask

To create a task as a Task node, simply sub-class this class or use ROSTask_template.py
"""
from taskforce_common import Task


class RosTask(Task):

    def onInit(self, message=None):
        return True
