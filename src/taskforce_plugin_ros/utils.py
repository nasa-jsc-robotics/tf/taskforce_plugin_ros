import os
from taskforce_common import logcfg


def configureRosLogging(configFile = None):
    """
    Configure rospy to use the taskforce common logging config file.

    Args:
        configFile:
    """
    if configFile is None:
        configFile = logcfg.get_log_config_file_path()
    os.environ['ROS_PYTHON_LOG_CONFIG_FILE'] = configFile
