"""
RosTask Template

To create a custom Task:
- change the name of the task
- (optional) add events to the 'events' variable
- (optional) add commands to the 'commands' variable
- (optional) add parameters to the 'parameters' variable
- (optional) insert custom code by replacing the "INSERT USER CODE HERE" comment blocks.
  Any methods that don't need to customize / overload, can safely be deleted.

To log messages, the component has a member logger which uses the python logger.
Logging is configured at the application level.  The Task Engine uses NASA's
nasa_common_logging module

    Example:
        self.logger.warning('Warning message!')
"""

# standard imports.  Add more imports as needed.
from taskforce_common import Task, TaskState, Response, ErrorResponse
from taskforce_plugin_ros import RosTask

import rospy


# Change the name of the task
class TEMPLATE_TASK_NAME(RosTask):
    """
    Events:
    -------
    Add emittable events as a list of strings in a
    class variable called "events".  By default,
    an event will be emitted after onStartup, onStop,
    and when a task completes.

    Example:
        events = ['error', 'trajComple', 'etc']

    To emit an event, call:
        self.emit(<event name>)

    Example:
        self.emit('complete')

    Commands:
    ---------
    Add custom commands by putting them method name in the "commands" array.  This method
    will now be callable through the Task's command handler.

    Example:
        commands = ['myMethod']

    Parameters:
    ----------
    Add custom parameters by adding values to the 'parameters' dictionary.  These values can be
    accessed at run-time by accessing 'self.parameters' or using the base class's 'setParameters' / 'getParameters'.

    NOTE: Do not add / delete parameters that have not been declared in the 'parameters' dictionary!

    Example:
        parameters = {'color' : 'blue', 'size' : 'large'}

    """
    events = []
    commands = []
    parameters = {}

    def __init__(self, name, eventPorts=[], commandPorts=[]):
        RosTask.__init__(self, name, eventPorts, commandPorts)

        ########################
        #  INSERT USER CODE HERE
        ########################

    def onInit(self, message=None):
        '''
        The onInit method will get called when the Task is initialized.  Typically,
        this only gets called once per deployment.  This is where you would register publishers / subscribers,
        or do one-time setup functions.  If init is successful, it should return True.
        On error, it should return False.
        '''
        RosTask.onInit(self, message)

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onStartup(self, message=None):
        '''
        The onStartup method will get called when a Task is started.  This is
        for functions that needs to be run everytime the Task is run, but
        maybe not in a continuous loop. If onStartup is successful, it should
        return True.  On error, it should return False.
        '''
        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onExecute(self, message=None):
        '''
        The onExecute method is the "main" loop of the Task.  When a task is
        started, if will first call onStartup.  If onStartup is successful,
        the onExecute method will be called in it's own thread.  If a task
        is currently running, more onExecute threads will NOT be created.
        '''

        # if onExecute needs to run continuously, put the code in the following loop
        #while (self.state == TaskState.RUNNING):

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onStop(self, message=None):
        '''
        The onStop method is called when a task is manually told to stop.
        '''

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onShutdown(self, message=None):
        '''
        The onShutdown method is called when a task is being removed completely.
        Code needed to clean up or special destructors should be placed here.
        '''
        RosTask.onShutdown(self)
        # here, we need to unregister all publishers and subscribers.
        # e.g.:
        # 	self.publisher.unregister()

        ########################
        #  INSERT USER CODE HERE
        ########################


        return True
