import json

import rospy
from taskforce_common import Publisher
from taskforce_plugin_ros_msgs.msg import Message

class RosPublisher(Publisher):
    """
    RosPublisher is a TaskForce Publisher port that utilizes the ROS transport.  Instances of this class can be used
    wherever a Publisher port can be used within TaskForce.  Outgoing messages will be of type:
    taskforce_plugin_ros_msgs.Message, and sent out on the ROS Topic defined in the constructor. To send a message using
    this publisher, create a taskforce_common.Message object and pass it to the "send" method.
    """

    def __init__(self, topic):
        super(Publisher, self).__init__()
        self.topic = topic
        self.initialized = False
        self.connected = False
        self.publisher = None

    def initialize(self):
        """
        Initialize the port.

        Returns:
            True on success.  If this method is called when already initialized, False will be returned.
        """
        if not self.initialized:
            self.initialized = True
            return True
        return False

    def connect(self):
        """
        Connect the Port.

        Returns:
            True on success.  If this method is called when already connected, False will be returned.
        """
        if not self.connected:
            if not self.initialized:
                init_ok = self.initialize()
                if not init_ok:
                    return False

            self.publisher = rospy.Publisher(self.topic, Message, queue_size=1)
            self.connected = True
            return True
        return False

    def disconnect(self):
        """
        Disconnect the publisher
        
         Returns:
            True on success.  If this method is called when already disconnected, False will be returned.
        """
        if self.connect:
            if self.publisher is not None:
                self.publisher.unregister()
                self.publisher = None
                self.connected = False
                return True
        return False

    def send(self, message):
        """
        Send a message out on a ROS topic.

        Args:
            message: taskforce_common.Message object
        """
        if not self.connected:
            connect_ok = self.connect()
            if not connect_ok:
                return False
        msg = Message()
        msg.name = str(message.name)
        msg.source = str(message.source)
        msg.destination = str(message.destination)
        msg.data = json.dumps(message.data)
        self.publisher.publish(msg)
