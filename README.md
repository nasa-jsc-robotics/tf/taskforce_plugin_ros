taskforce_plugin_ros
====================

This plugin is used to extend TaskForce to use with ROS.  To make TaskForce ROS-compatible, simply run with
the provided launch file:

[launch/task_engine_ros.launch](./launch/task_engine_ros.launch)


        
