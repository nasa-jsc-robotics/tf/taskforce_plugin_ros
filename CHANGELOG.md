Change Log
==========

2.0.2
-----

* Cleaned out unused code from plugin

2.0.1
-----

* Updated the RosTask template, which simplifies it's implementation
* Converted tests to rostests
* Added NOSA LICENSE.md file

2.0.0
-----

* Added RosTaskEngineRosNode to act as the ROS node for TaskForce
* Converted RosPublisher and RosServer to run in the main process
* Removed trusty CI

1.0.2
-----

* Added xenial CI
