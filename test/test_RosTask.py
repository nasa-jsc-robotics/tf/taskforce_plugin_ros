#!/usr/bin/env python

import unittest
import sys
from threading import Thread
from taskforce_plugin_ros import RosTask
from taskforce_common import PipeReqRep
from taskforce_common import Command, TaskState
import time

import rospy, rostest
from sensor_msgs.msg import JointState

PKG='taskforce_plugin_ros'


class RosTaskSubClass(RosTask):

    commands = ['getMsgCount']

    def onInit(self, message=None):
        self.publisher = rospy.Publisher('/joint_state', JointState, queue_size=10)
        self.subscriber = rospy.Subscriber('/joint_state', JointState, self.getMsg, queue_size=10)
        self.joint_states_sent = 0
        self.joint_states_received = 0
        return True

    def onShutdown(self, message=None):
        self.publisher.unregister()
        self.subscriber.unregister()
        return True

    def onExecute(self, message=None):
        self.logger.info('publishing joint state message')
        msg = JointState()
        msg.name.append('joint0')
        msg.header.stamp = rospy.Time.now()
        msg.position.append(1.0)
        msg.velocity.append(2.0)
        msg.effort.append(3.0)
        self.publisher.publish(msg)
        return True

    def getMsg(self,msg):
        self.logger.info('message received!')
        self.joint_states_received = self.joint_states_received + 1

    def getMsgCount(self, message=None):
        return self.joint_states_received


class TestRosTask(unittest.TestCase):

    def setUp(self):
        self.messages_receieved = 0
        self.cmdConn = PipeReqRep()
        self.serverPort = self.cmdConn.getServer()
        self.clientPort = self.cmdConn.getClient()

    def testTopicUnsubscribe(self):
        '''
        test that topics get unregistered with ROS core on shutdown
        '''
        task = RosTaskSubClass('testTopicUnsubscribe')

        task.deploy()
        task.init()
        # wait for publishers / subscribers to get registered
        time.sleep(0.1)

        topics = rospy.get_published_topics()
        self.assertTrue(['/joint_state', 'sensor_msgs/JointState'] in topics)

        task.shutdown()
        time.sleep(0.1)
        topics = rospy.get_published_topics()
        self.assertFalse(['/joint_state', 'sensor_msgs/JointState'] in topics)

    def testPublish(self):
        '''
        Test sending ROS message from one Task to another
        '''
        task1 = RosTaskSubClass('testPublish')
        task2 = RosTaskSubClass('testSubscribe')

        # deploy the tasks
        task1.deploy()
        task2.deploy()

        task1.init()
        task2.init()

        time.sleep(0.1)
        topics = rospy.get_published_topics()
        self.assertTrue(['/joint_state', 'sensor_msgs/JointState'] in topics)

        task1.startup()

        success = False
        timeout = 1
        start_t = time.time()
        while(time.time() - start_t < timeout):
            response = task2.getMsgCount()
            print response
            if response > 0:
                success = True
                break
            time.sleep(0.1)
        self.assertTrue(success)

        task1.shutdown()
        task2.shutdown()

    def getRosMessage(self, msg):
        '''
        Call
        '''
        print 'here'
        self.messages_receieved = self.messages_receieved + 1


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    rospy.init_node('test_RosTask')
    rostest.rosrun(PKG, 'test_RosTask', TestRosTask, sys.argv)
