#!/usr/bin/env python

import unittest
import sys
import rospy, rostest
from multiprocessing import Process
import time

from taskforce_plugin_ros import RosServer
from taskforce_common import Message
from rospy import ROSException
from taskforce_plugin_ros_msgs.srv import *

PKG = 'taskforce_plugin_ros'


class TestRosServer(unittest.TestCase):

    def test_initialize(self):
        server = RosServer('TestServer')
        self.assertTrue(server.initialize())
        self.assertFalse(server.initialize())

    def test_connectAndDisconnect(self):
        server = RosServer('TestServer')
        self.assertRaises(ROSException, rospy.wait_for_service, '/TestServer', 0.01)
        server.connect()
        rospy.wait_for_service('/TestServer', 2)

        # test connecting when connected
        self.assertFalse(server.connect())
        self.assertTrue(server.initialized)

        server.disconnect()

        self.assertFalse(server.connected)
        self.assertTrue(server.initialized)
        self.assertRaises(ROSException, rospy.wait_for_service, '/TestServer', 0.01)

        self.assertEqual(None, server.serviceProcess)

    def test_waitForRequestAndReply(self):
        server = RosServer('TestServer')
        req = server.waitForRequest()
        # test freshly started service has no requests
        self.assertEqual(None, req)

        rospy.wait_for_service('/TestServer')

        def serviceCall():
            """
            This process will call the service and check the response
            """
            serviceProxy = rospy.ServiceProxy('/TestServer', Command)
            resp = serviceProxy(name='testName',
                                source='testSource',
                                destination='testDest',
                                args='["arg1","arg2",3]',
                                kwargs='{"name":"arg1","val":1.234}')

            self.assertEqual('response', resp.name)
            self.assertEqual('src', resp.source)
            self.assertEqual('dst', resp.destination)
            self.assertEqual('{"int_val": 1, "str_val": "boo"}', resp.data)

        srvProcess = Process(target=serviceCall)
        srvProcess.start()

        req = None
        while(req is None):
            time.sleep(0.1)
            req = server.waitForRequest()
            server.reply(Message('response', source='src', destination='dst', data={"int_val": 1, "str_val": "boo"}))

        self.assertEqual('testName', req.name)
        self.assertEqual('testSource', req.source)
        self.assertEqual('testDest', req.destination)
        self.assertEqual(["arg1", "arg2", 3], req.args)
        self.assertEqual({"name": "arg1", "val": 1.234}, req.kwargs)
        srvProcess.join()


if __name__ == "__main__":
    rospy.init_node('test_RosServer')
    rostest.rosrun(PKG, 'test_RosServer', TestRosServer, sys.argv)
