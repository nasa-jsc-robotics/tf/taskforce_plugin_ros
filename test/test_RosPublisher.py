#!/usr/bin/env python

import unittest
import rospy, rostest

import sys
from multiprocessing import Process
import time

from taskforce_common import Message
from taskforce_plugin_ros_msgs.msg import Message as MessageTopic
from taskforce_plugin_ros import RosPublisher

PKG='taskforce_plugin_ros'


class TestRosPublisher(unittest.TestCase):

    def test_initialize(self):
        publisher = RosPublisher('testTopic')
        self.assertTrue(publisher.initialize())
        self.assertFalse(publisher.initialize())
        self.assertTrue(publisher.initialized)
        self.assertFalse(publisher.connected)

    def test_connect(self):
        name = 'test_topic'
        topic = '/' + name
        publisher = RosPublisher(name)

        self.assertTrue(publisher.connect())

        found = False
        timeout = 10
        start_t = time.time()
        while(time.time() - start_t < timeout):
            topics = rospy.get_published_topics()[0]
            if topic in topics:
                found = True
                break
            time.sleep(0.1)

        self.assertTrue(found, "{}".format('Topic not found'))

    def test_send(self):
        topic = '/test_send_topic'

        def subscriber():
            self.running = True

            def msg_cb(msg):
                self.assertEqual('topicName',msg.name)
                self.assertEqual('topicSource',msg.source)
                self.assertEqual('topicDst',msg.destination)
                self.assertEqual('{"int_var": 1, "str_var": "foo"}', msg.data)
                rospy.loginfo('msg received')
                self.running = False

            rospy.init_node('subscriber')
            rospy.Subscriber(topic, MessageTopic,msg_cb)

            while(self.running):
                time.sleep(0.1)

        subscriberProcess = Process(target=subscriber)
        subscriberProcess.start()

        publisher = RosPublisher(topic)
        while(subscriberProcess.is_alive()):
            publisher.send(Message('topicName','topicSource','topicDst',data={"int_var":1,"str_var":"foo"}))
            time.sleep(0.1)

        subscriberProcess.join()


if __name__ == "__main__":
    rospy.init_node('test_RosPublisher')
    rostest.rosrun(PKG, 'test_RosPublisher', TestRosPublisher, sys.argv)
