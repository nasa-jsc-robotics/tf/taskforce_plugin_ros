import rospy
from taskforce_plugin_ros import RosServer
from taskforce_plugin_ros_msgs.msg import Message
from taskforce_common import Response
import time

if __name__ == "__main__":
    rospy.init_node('RosServerTest')

    server = RosServer('ROSService')
    server.initialize()
    server.connect()

    while not rospy.is_shutdown():
        request = server.waitForRequest()
        if request is not None:
            print request
            server.reply(Response(name="sending reply", source="RosServerTest"))
        time.sleep(0.1)

    rospy.spin()